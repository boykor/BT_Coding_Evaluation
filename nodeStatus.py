#The program is supposed to print out a status report for 
#the nodes from a file depending on the messages that were
#received at the master
#===============================================================
#Boyko Radulov                                                 ||
#07/02/2018                                                    ||
#===============================================================
import sys
try:
    filename = sys.argv[1]
except IndexError:
    print "Please provide a file"
    exit(1)
try:
    with open(filename) as f:
        line = f.readline()
        ls = []
        while (line != ""):
            ls.append(line[:-1].split())
            line = f.readline()
        ls.reverse()
        seenSoFar =[]
        try:
            for element in ls:
                if element[2] not in seenSoFar:
                    commonOutputStr = element[0] + " " + element[2] + " " + element[3]
                    if element[3] == "HELLO":
                        seenSoFar.append(element[2])
                        print element[2], "ALIVE" , commonOutputStr
                    elif element[3] == "FOUND":
                        seenSoFar.append(element[2])
                        print element[4], "ALIVE", commonOutputStr,element[4]
                    elif element[3] == "LOST":
                        print element[2], "ALIVE",commonOutputStr,element[4]
                        print element[4], "DEAD",commonOutputStr,element[4]
                        seenSoFar.append(element[4])
        except IndexError:
            print "Malformed text file"
            exit(2)
except IOError:
    print "File does not exit"
    exit(3)
